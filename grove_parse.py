from grove_lang import *

# Utility methods for handling parse errors
def check(condition, message = "Unexpected end of expression"):
    """ Checks if condition is true, raising a GroveError otherwise """
    if not condition:
        raise GroveError("GROVE: " + message)

def expect(token, expected):
    """ Checks that token matches expected
        If not, throws a GroveError with explanatory message """
    if token != expected:
        check(False, "Expected '" + expected + "' but found '" + token + "'")

def is_expr(x):
    if not isinstance(x, Expr):
        check(False, "Expected expression but found " + str(type(x)))

# Checking for integer
def is_int(s):
    """ Takes a string and returns True if in can be converted to an integer """
    try:
        int(s)
        return True
    except ValueError:
        return False

def is_valid_str(s):
    """ Takes a string and returns true if it does not contain whitespace or quotes """
    # Cases that make the string syntax incorrect
    for case in ["\"" in s, "'" in s, " " in s, "\n" in s, "\t" in s, "\r" in s, "," in s]:
        if case:
            return False
        return True

def parse(s):
    """ Return an object representing a parsed command
        Throws GroveError for improper syntax """
    (root, remaining_tokens) = parse_tokens(s.split())
    check(len(remaining_tokens) == 0, "Expected end of command but found '" + " ".join(remaining_tokens) + "'")
    return root

def is_valid_name(s):
    """ Checks for the correct Name syntax; first character is
    alphabetic and all other are alphanumeric"""
    return( s[0].isalpha() or s[0] == '_' and all(c.isalnum() or c == '_' for c in start[1:]) )

def parse_tokens(tokens):
    """ Returns a tuple:
        (an object representing the next part of the expression,
         the remaining tokens)
    """

    check(len(tokens) > 0)

    start = tokens[0]

    if is_int(start):
        # a number
        return( Num(int(start)), tokens[1:])

    elif start[0] == '"':
        expect(start[-1:],'"')
        check( is_valid_str(start[1:-1]), "String contains invalid characters" )
        return ( Str(start[1:-1]), tokens[1:] )
        pass

    elif start == 'quit' or start == 'exit':
        return ( Exit(), tokens[1:] )

    elif start == '+':
        # An addition
        expect( tokens[1], '(' )
        (child1, tokens) = parse_tokens( tokens[2:] )
        check(len(tokens) > 1)
        expect(tokens[0], ')')
        expect(tokens[1], '(')
        (child2, tokens) = parse_tokens( tokens[2:] )
        check(len(tokens) > 0)
        expect(tokens[0], ')')

        return( Addition(child1, child2), tokens[1:] )

    elif start == 'set':
        # An assignment statement
        check(len(tokens) > 3)
        (varname, tokens) = parse_tokens(tokens[1:])
        expect(tokens[0], '=')
        (next, tokens) = parse_tokens(tokens[1:])
        is_expr(next)
        return (Stmt(varname, next), tokens)

    elif start == 'call':
        check( len(tokens) > 4 )
        expect( tokens[1], '(' )
        object_name = tokens[2]
        function_name = tokens[3]
        args = []
        tokens = tokens[4:]
        while tokens[0] != ')':
            (arg, tokens) = parse_tokens(tokens)
            check(not isinstance(arg, Stmt))
            args.append(arg)
        return( Method(object_name, function_name, *args), tokens[1:] )

    elif start == 'new':
        check(len(tokens) > 0)
        args = tokens[1].split('.')
        check(len(args) > 0)
        if len(args) == 1:
            return (Object(className = args[0]), tokens[2:])
        else:
            return (Object(args[1],args[0]), tokens[2:])

    elif start == 'import':
        check( len(tokens) == 2 )
        return ( Import(tokens[1]), tokens[2:] )

    else:
        check( is_valid_name(tokens[0]) , "Variable names must start with alphabetic characters and contain only alphanumeric characters otherwise" )
        check( ',' not in tokens[0] and '"' not in tokens[0], 'Variable names cannot contain " or , ' )
        return( Name(tokens[0]), tokens[1:] )

if __name__ == "__main__":
    pass

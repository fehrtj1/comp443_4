## Parse tree nodes for the Grove language
from grove_error import *
from types import *
import sys
import importlib
import builtins

var_table = {}

class Expr:
    pass # lets compiler know that this block will be empty without throwing an error

class Num(Expr):
    def __init__(self, value):
        self.value = value

    def eval(self):
        return self.value


class Addition(Expr):
    def __init__(self, child1, child2):
        self.child1 = child1
        self.child2 = child2

        if not isinstance(self.child1, Expr):
            raise GroveError("GROVE: expected expression but received" + str(type(self.child1)))

        if not isinstance(self.child2, Expr):
            raise GroveError("GROVE: expected expression but received" + str(type(self.child2)))

    def eval(self):
        if not type(self.child1.eval()) == type(self.child2.eval()):
            raise GroveError("GROVE: addition incompatible with types " + str(type(self.child1)) + " and " + str(type(self.child2)))

        return self.child1.eval() + self.child2.eval()


class Name(Expr):
    def __init__(self, name):
        self.name = name

    def getName(self):
        return self.name

    def eval(self):
        if self.name in var_table:
            return var_table[self.name]
        else:
            raise GroveError("GROVE: undefined variable " + self.name)

class Str(Expr):
    def __init__(self, stringname):
        self.stringname = stringname

    def eval(self):
        return self.stringname

class Method(Expr):
    def __init__(self, obj_name, method_name, *args):
        self.obj_name = obj_name
        self.method_name = method_name
        self.args = args

    def eval(self):
        if self.obj_name not in var_table:
            raise GroveError("GROVE: Object " + str(self.obj_name) + " undefined")
        obj = var_table[self.obj_name]
        if not hasattr(obj, self.method_name):
            raise GroveError("GROVE: Object " + str(self.obj_name) + " does not have a method " + str(self.method_name))

        if len(self.args) > 0: # 1 or more arguments
            vals = [s.eval() for s in self.args]
            return getattr(var_table[self.obj_name], self.method_name)(*vals)
        else: # 0 arguments
            return getattr(var_table[self.obj_name], self.method_name)()

class Exit(Expr):
    def eval(self):
        sys.exit(0)

class Import(Expr):
    def __init__(self, module):
        self.module = module

    def eval(self):
        try:
            m = importlib.import_module(self.module)
            globals()[self.module] = m
        except Exception:
            raise GroveError("GROVE: " + str(self.module) + " does not exist.")


class Object(Expr):
    def __init__(self, className, module=None,):
        self.moduleName = module
        self.className = className

    def eval(self):
        if self.moduleName is None:
            obj = globals()[self.className]
            return obj()
        else:
            module = globals()[self.moduleName]
            if type(module) == ModuleType:
                obj = getattr(module, self.className)
            else:
                obj = module[self.className]
            return obj()


class Stmt:
    def __init__(self, varname, expr):
        self.varname = varname
        self.expr = expr

        if not isinstance(self.expr, Expr):
            raise GroveError("GROVE: expected expression but received " + str(type(self.expr)))

        if not isinstance(self.varname, Name):
            raise GroveError("GROVE: expected Name but received " + str(type(self.varname)))

    def eval(self):
        try:
            var_table[self.varname.getName()] = self.expr.eval()
        except Exception:
            raise GroveError("GROVE: " + str(self.varname) + " or " + str(self.expr) + " does not exist.")

# Grove is dynamically typed. The user must only use the "set" keyword in order declare a variable,
# and the interpreter parses it and determines its type at runtime. Additionally, the user does not specify
# a type when declaring a variable, it is assigned by the interpreter. The user can change a variable that
# stores an int to one that stores a string.

from grove_parse import *

if __name__ == "__main__":
    while True:
        ln = input("Grove>> ")
        try:
            root = parse(ln)
            res = root.eval()
            if not res is None:
                print(res)
        except GroveError as e:
            print( str(e) )
